<?php
    /*Template Name: profil_satker
    Theme Name : bwskal2*/
?>
<?php include("header.php");
 
global $post;

//Custom query untuk wordpress
//$my_wp_query = new WP_Query();
//$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 's' => 'satuankerja-', 'orderby' => 'ID', 'order' => 'ASC'));
// Filter through all pages and find page's children
//$children = get_page_children($post->ID, $all_wp_pages );


$children = new WP_Query(array('post_type' => 'page', 'post_parent' => $post->ID, 'orderby' => 'ID', 'order' => 'ASC'));

//$children = get_pages(array('parent' => $post->ID, 'sort_column' => 'ID', 'sort_order' => 'asc'));

//echo '<pre>';print_r($post); print_r($my_wp_query);exit;

?>

<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="profil">
							<div class="container">
                                <div class="row-fluid page-title">
                                    <h3>PROFIL SATUAN KERJA BALAI WILAYAH SUNGAI KALIMANTAN II</h3>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3 sidebar-content">
                                        <ul class="ver-inline-menu tabbable margin-bottom-25">
                                        	<?php $jmlhTab = 1; ?>
                                        	<?php //foreach ($children as $index => $row) : ?>
                                            <?php while ( $children->have_posts() ) : $children->the_post() ?>
                                        	<?php
                                        		$tab = '#tab_'.$jmlhTab;
                                        		$id = strtolower(str_replace(' ', '', $post->post_title)).'_satker';
                                        	?>
                							<li class="<?php echo $jmlhTab==1 ? 'active' : ''; ?>">
                								<a href="<?php echo $tab; ?>" data-toggle="tab" id="<?php echo $id; ?>">
                									<i class="icon-briefcase"></i>
                									<?php echo ucwords($post->post_title); ?>
                								</a> 
                								<span class="after"></span>                                    
                							</li>
                							<?php
                								$jmlhTab++;
                								//endforeach;
                                                endwhile;
                							?>
                						</ul>
                                    </div>
                                    <div class="span9 tab-content profil-content">
                                        <div class="tab-content">
                                        	<?php $jmlhTab = 1; ?>
                                        	<?php //foreach ($children as $index => $row) : ?>
                                            <?php while ( $children->have_posts() ) : $children->the_post() ?>
                                        	<?php
                                        		$tab = 'tab_'.$jmlhTab;
                                        	?>
                                            <div class="tab-pane <?php echo $jmlhTab==1 ? 'active' : ''; ?>" id="<?php echo $tab; ?>">
                                                <h3><?php echo strtoupper($post->post_title); ?></h3>
                                                <p><?php the_content(); ?></p>
                                            </div>
                                            <?php
                								$jmlhTab++;
                								//endforeach;
                                                endwhile;
                							?>
                                        </div>
                                    </div>
                                </div>     
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<?php include("footer.php") ?>