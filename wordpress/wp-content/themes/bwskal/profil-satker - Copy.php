<?php
    /*Template Name: profil_satker
    Theme Name : bwskal2*/
?>
<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="profil">
							<div class="container">
                                <div class="row-fluid page-title">
                                    <h3>PROFIL SATUAN KERJA BALAI WILAYAH SUNGAI KALIMANTAN II</h3>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3 sidebar-content">
                                        <ul class="ver-inline-menu tabbable margin-bottom-25">
                							<li class="active">
                								<a href="#tab_1" data-toggle="tab" id="informasi_satker">
                								<i class="icon-briefcase"></i> 
                								Informasi Umum
                								</a> 
                								<span class="after"></span>                                    
                							</li>
                							<li><a href="#tab_2" data-toggle="tab" id="struktur_satker"><i class="icon-leaf"></i> Struktur</a></li>
                							<li><a href="#tab_3" data-toggle="tab" id="produk_satker"><i class="icon-info-sign"></i> Produk</a></li>
                							<li><a href="#tab_4" data-toggle="tab" id="paket_satker"><i class="icon-tint"></i> Paket Pekerjaan</a></li>
                							<li><a href="#tab_5" data-toggle="tab" id="lain_satker"><i class="icon-plus"></i> Lain - lain</a></li>
                						</ul>
                                    </div>
                                    <div class="span9 tab-content profil-content">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <?php query_posts('category_name="pjsa-informasi-umum"');
                                                while (have_posts()) : the_post();?>
                                                <h3> <?php the_title(); ?> </h3>
                                                <p><?php the_content(); ?></p>
                                                <?php
                                                endwhile;
                                                ?>
                                                <h4>Kontak</h4>
                                    	        <address>
                                    			     <strong>Balai Wilayah Sungai Kalimantan II</strong><br>
                                    			     Jalan Tambun Bungai No.25<br>
                                    			     Kuala Kapuas, Kalimantan Tengah 731514<br>
                                    			     <abbr title="Telepon">Telp :</abbr> (0513) 22085<br />
                                                     <abbr title="Fax">Fax :</abbr> (0513) 22086
                                    			</address>
                                    			<address>
                                    				<strong>Email</strong><br>
                                    				<a href="mailto:#">bwskal2@gmail.com</a>
                                    			</address>
                                            </div>
                                            <div class="tab-pane" id="tab_2">
                                                <h3>STRUKTUR SATUAN KERJA</h3>
                                                <p>shahads</p>
                                            </div>
                                            <div class="tab-pane" id="tab_3">
                                                <h3>PRODUK</h3>
                                                <!-- BEGIN PRODUCT LIST -->
                                                <div class="portlet produk-list">
                                                    <div class="portlet-body flip-scroll">
                                                        <table class="table-bordered table-striped table-condensed flip-content">
                        									<thead class="flip-content">
                        										<tr>
                        											<th>Kode</th>
                        											<th>Produk</th>
                        											<th class="numeric">Pelaksana</th>
                        											<th class="numeric">Tgl Pembangunan</th>
                        											<th class="numeric">Lokasi</th>
                        											<th class="numeric">Open</th>
                        											<th class="numeric">High</th>
                        											<th class="numeric">Low</th>
                        											<th class="numeric">Volume</th>
                        										</tr>
                        									</thead>
                        									<tbody>
                        										<tr>
                        											<td>AAC</td>
                        											<td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                        											<td class="numeric">$1.38</td>
                        											<td class="numeric">-0.01</td>
                        											<td class="numeric">-0.36%</td>
                        											<td class="numeric">$1.39</td>
                        											<td class="numeric">$1.39</td>
                        											<td class="numeric">$1.38</td>
                        											<td class="numeric">9,395</td>
                        										</tr>
                        										<tr>
                        											<td>AAD</td>
                        											<td>ARDENT LEISURE GROUP</td>
                        											<td class="numeric">$1.15</td>
                        											<td class="numeric">  +0.02</td>
                        											<td class="numeric">1.32%</td>
                        											<td class="numeric">$1.14</td>
                        											<td class="numeric">$1.15</td>
                        											<td class="numeric">$1.13</td>
                        											<td class="numeric">56,431</td>
                        										</tr>
                        										<tr>
                        											<td>AAX</td>
                        											<td>AUSENCO LIMITED</td>
                        											<td class="numeric">$4.00</td>
                        											<td class="numeric">-0.04</td>
                        											<td class="numeric">-0.99%</td>
                        											<td class="numeric">$4.01</td>
                        											<td class="numeric">$4.05</td>
                        											<td class="numeric">$4.00</td>
                        											<td class="numeric">90,641</td>
                        										</tr>
                        										<tr>
                        											<td>ABC</td>
                        											<td>ADELAIDE BRIGHTON LIMITED</td>
                        											<td class="numeric">$3.00</td>
                        											<td class="numeric">  +0.06</td>
                        											<td class="numeric">2.04%</td>
                        											<td class="numeric">$2.98</td>
                        											<td class="numeric">$3.00</td>
                        											<td class="numeric">$2.96</td>
                        											<td class="numeric">862,518</td>
                        										</tr>
                        										<tr>
                        											<td>ABP</td>
                        											<td>ABACUS PROPERTY GROUP</td>
                        											<td class="numeric">$1.91</td>
                        											<td class="numeric">0.00</td>
                        											<td class="numeric">0.00%</td>
                        											<td class="numeric">$1.92</td>
                        											<td class="numeric">$1.93</td>
                        											<td class="numeric">$1.90</td>
                        											<td class="numeric">595,701</td>
                        										</tr>
                        										<tr>
                        											<td>ABY</td>
                        											<td>ADITYA BIRLA MINERALS LIMITED</td>
                        											<td class="numeric">$0.77</td>
                        											<td class="numeric">  +0.02</td>
                        											<td class="numeric">2.00%</td>
                        											<td class="numeric">$0.76</td>
                        											<td class="numeric">$0.77</td>
                        											<td class="numeric">$0.76</td>
                        											<td class="numeric">54,567</td>
                        										</tr>
                        										<tr>
                        											<td>ACR</td>
                        											<td>ACRUX LIMITED</td>
                        											<td class="numeric">$3.71</td>
                        											<td class="numeric">  +0.01</td>
                        											<td class="numeric">0.14%</td>
                        											<td class="numeric">$3.70</td>
                        											<td class="numeric">$3.72</td>
                        											<td class="numeric">$3.68</td>
                        											<td class="numeric">191,373</td>
                        										</tr>
                        										<tr>
                        											<td>ADU</td>
                        											<td>ADAMUS RESOURCES LIMITED</td>
                        											<td class="numeric">$0.72</td>
                        											<td class="numeric">0.00</td>
                        											<td class="numeric">0.00%</td>
                        											<td class="numeric">$0.73</td>
                        											<td class="numeric">$0.74</td>
                        											<td class="numeric">$0.72</td>
                        											<td class="numeric">8,602,291</td>
                        										</tr>
                        										<tr>
                        											<td>AGG</td>
                        											<td>ANGLOGOLD ASHANTI LIMITED</td>
                        											<td class="numeric">$7.81</td>
                        											<td class="numeric">-0.22</td>
                        											<td class="numeric">-2.74%</td>
                        											<td class="numeric">$7.82</td>
                        											<td class="numeric">$7.82</td>
                        											<td class="numeric">$7.81</td>
                        											<td class="numeric">148</td>
                        										</tr>
                        										<tr>
                        											<td>AGK</td>
                        											<td>AGL ENERGY LIMITED</td>
                        											<td class="numeric">$13.82</td>
                        											<td class="numeric">  +0.02</td>
                        											<td class="numeric">0.14%</td>
                        											<td class="numeric">$13.83</td>
                        											<td class="numeric">$13.83</td>
                        											<td class="numeric">$13.67</td>
                        											<td class="numeric">846,403</td>
                        										</tr>
                        										<tr>
                        											<td>AGO</td>
                        											<td>ATLAS IRON LIMITED</td>
                        											<td class="numeric">$3.17</td>
                        											<td class="numeric">-0.02</td>
                        											<td class="numeric">-0.47%</td>
                        											<td class="numeric">$3.11</td>
                        											<td class="numeric">$3.22</td>
                        											<td class="numeric">$3.10</td>
                        											<td class="numeric">5,416,303</td>
                        										</tr>
                        									</tbody>
                        								</table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_4">
                                                <h3>PAKET PEKERJAAN</h3>
                                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                        						<div class="portlet paket-list">
                        							<div class="portlet-body">
                        								<table class="table table-striped table-bordered table-advance table-hover">
                        									<thead>
                        										<tr>
                        											<th><i class="icon-briefcase"></i> Pekerjaan</th>
                        											<th class="hidden-phone"><i class="icon-user"></i> Lokasi</th>
                        											<th><i class="icon-shopping-cart"></i> Total</th>
                        											<th></th>
                        										</tr>
                        									</thead>
                        									<tbody>
                        										<tr>
                        											<td class="highlight">Proyek 1</td>
                        											<td class="hidden-phone">Mike Nilson</td>
                        											<td>2560.60$</td>
                        											<td><a href="#" class="btn mini blue"><i class="icon-share"></i> Lelang</a></td>
                        										</tr>
                        										<tr>
                        											<td class="highlight">Proyek 2</td>
                        											<td class="hidden-phone">Adam Larson</td>
                        											<td>560.60$</td>
                        											<td><a href="#" class="btn mini blue"><i class="icon-share"></i> Lelang</a></td>
                        										</tr>
                        										<tr>
                        											<td class="highlight">Proyek 3</td>
                        											<td class="hidden-phone">Daniel Kim</td>
                        											<td>3460.60$</td>
                        											<td><a href="#" class="btn mini blue"><i class="icon-share"></i> Lelang</a></td>
                        										</tr>
                        										<tr>
                        											<td class="highlight">Proyek 4</td>
                        											<td class="hidden-phone">Nick </td>
                        											<td>2560.60$</td>
                        											<td><a href="#" class="btn mini blue"><i class="icon-share"></i> Lelang</a></td>
                        										</tr>
                        									</tbody>
                        								</table>
                        							</div>
                        						</div>
                        						<!-- END SAMPLE TABLE PORTLET-->
                                            </div>
                                            <div class="tab-pane" id="tab_5">
                                                <p>cvb</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>     
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<?php include("footer.php") ?>