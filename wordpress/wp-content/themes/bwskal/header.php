<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="<?php bloginfo('charset')?>"/>
	<title><?php bloginfo('name')?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="nicky" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php bloginfo('template_directory')?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/default.css" rel="stylesheet" type="text/css"/>
	<link href="<?php bloginfo('template_directory')?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <!--<link href="<?php bloginfo('template_directory')?>/css/base-clndr.less" rel="stylesheet" type="text/css"/>-->
    <link href="<?php bloginfo('template_directory')?>/css/mini-clndr.less" rel="stylesheet/less" type="text/css"/>
    <!--<link href="<?php bloginfo('template_directory')?>/css/clndr1.less" rel="stylesheet" type="text/css"/>-->
	<!-- END GLOBAL MANDATORY STYLES -->
    <?php wp_head();?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-full-width">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-custom">
			<div class="container-fluid">
                <!-- BEGIN RESPONSIVE MENU TOGGLER 
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php bloginfo('template_directory')?>/img/menu-toggler.jpg" alt="" />
				</a>          -->
				<!-- END RESPONSIVE MENU TOGGLER -->      
				<div class="collapse nav-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="unstyled" style="float: right;">
                        <li><a class="btn blue login-button" href="<?php bloginfo('url')?>/wp-admin">Login</a></li>
                    </ul>
    			</div><!-- /.navbar-collapse -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
    <div class="banner">
            <div class="row-fluid">
                 <div class="span12">
                    <div class="container">
                        <a href="<?php bloginfo('url')?>"><img src="<?php bloginfo('template_directory')?>/img/logo.png" alt=""/></a>
                    </div>
                 </div>
            </div>
    </div> 
    <div class="header navbar navbar-custom">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner-default">
			<div class="container-fluid">
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php bloginfo('template_directory')?>/img/menu-toggler.jpg" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->      
				<!-- BEGIN TOP NAVIGATION MENU -->  
                <div class="nav hor-menu nav-collapse collapse"> 
                    <div class="container">
                        <ul class="nav" id="nav2">
                            <li class="visible-phone visible-tablet">
    								<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    								<form class="sidebar-search">
    									<div class="input-box">
    										<a href="javascript:;" class="remove"></a>
    										<input type="text" placeholder="Search..." />            
    										<input type="button" class="submit" value=" " />
    									</div>
    								</form>
    								<!-- END RESPONSIVE QUICK SEARCH FORM -->
    				        </li>
                            <?php
    				        	//Custom query untuk wordpress
								$my_wp_query = new WP_Query();
    				        							
								//Ambil menu root
								$menuRoot = get_pages(array('parent' => 0, 'sort_column' => 'menu_order', 'sort_order' => 'asc'));
                                
                                //echo '<pre>';print_r($menuRoot);exit;
							?>
							<?php foreach ($menuRoot as $index => $row) : ?>
							
							<?php
								$wp_pages_by_query = $my_wp_query->query(array('post_type' => 'page', 's' => $row->post_content, 'orderby' => 'ID', 'order' => 'ASC'));
								$menuChildren = get_page_children($row->ID, $wp_pages_by_query);
							?>
							
                            <?php if ($menuChildren!=NULL) : ?>
                            <li class="dropdown">
	                            <a href="#" data-toggle="dropdown"><?php echo strtoupper($row->post_title); ?> <i class="icon-angle-down"></i></a>
	                            <ul class="dropdown-menu" role="menu">
	                            <?php foreach ($menuChildren as $i => $r) : ?>
	    							<li>
	    								<a href="<?php echo bloginfo('url').'/?page_id='.$r->ID; ?>">
	    									<?php
	    										$title = explode('-', $r->post_title); 
	    										echo strtoupper($title[1]);
	    									?>
	    								</a>
	    							</li>
	                            <?php endforeach; ?>
	                            </ul>
                           	</li>
                           	<?php else : ?>
                           	<li>
                            	<a href="<?php echo bloginfo('url').'/?page_id='.$row->ID; ?>"><?php echo strtoupper($row->post_title); ?></a>
                            </li>
                            <?php endif; ?>
                            	
							<?php endforeach; ?>
                            
                            <li>
                                <span class="hor-menu-search-form-toggler">&nbsp;</span>
								<div class="search-form hidden-phone hidden-tablet">
									<form class="form-search">
										<div class="input-append">
											<input type="text" placeholder="Search..." class="m-wrap">
											<button type="button" class="btn"></button>
										</div>
									</form>
								</div>
                            </li>
                        </ul>
                    </div>
                </div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>               
	<!-- END HEADER -->