<?php
    /*Template Name: Home
    Theme Name : bwskal2*/
?>
<?php get_header(); ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid relative">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="myCarousel">
							<div class="container height-inherit">
								<div id="promo_carousel" class="carousel slide height-inherit">
									<div class="carousel-inner inherit">
										<div class="active item inherit">
											<div class="row-fluid inherit">
												<!--<div class="span7 margin-bottom-20 margin-top-20 animated rotateInUpRight">
													<h1>Welcome to Metronic..</h1>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.
													Lunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
													<a href="index.html" class="btn red big xlarge">
													Take a tour
													<i class="m-icon-big-swapright m-icon-white"></i>                                
													</a>
												</div>-->
												<div class="span12 animated rotateInDownLeft">
													<a href="index.html"><img src="<?php bloginfo('template_directory')?>/img/hutan-kahayan-1.jpg" alt=""></a>
												</div>
											</div>
										</div>
										<div class="item">
											<div class="row-fluid">
												<div class="span5 animated rotateInUpRight">
													<a href="index.html"><img src="<?php bloginfo('template_directory')?>/img/img1_3.png" alt=""></a>
												</div>
												<div class="span7 margin-bottom-20 animated rotateInDownLeft">
													<h1>BWSKAL2 on Android</h1>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
													<p>Lunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
													<a href="index.html" class="btn green big xlarge">
													But it today
													<i class="m-icon-big-swapright m-icon-white"></i>                                
													</a>
												</div>
											</div>
										</div>
									</div>
									<a class="carousel-control left" href="#promo_carousel" data-slide="prev">
									<i class="m-icon-big-swapleft m-icon-white"></i>
									</a>
									<a class="carousel-control right" href="#promo_carousel" data-slide="next">
									<i class="m-icon-big-swapright m-icon-white"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="newsticker">
							<div class="container">
                                <div  class="ticker">
									<ul>
                                        <?php 
                                            $query = new WP_Query(array(
                                                'post_type' => 'berita',
                                                'date_query' => array(
                                            		array(
                                            			'year' => date( 'Y' ),
                                            			'week' => date( 'W' ),
                                            		),
                                            	),
                                            ));
                                        ?>
                                        <?php //echo "<pre>"; print_r($query); exit;?>
                                        <?php if ( $query->have_posts() ) : ?>
                                        <?php while ( $query->have_posts( ) ) : $query->the_post(); ?>
                                            <li><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></li>
                                        <?php endwhile; ?>
                                        <?php else : ?>
                                        <li style="padding-bottom: 0px;"><p><?php echo "Sorry, no posts matched your criteria."; ?></p></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
						</div>
						<div class="news">
							<div class="container height-inherit">
								<div class="row-fluid height-inherit">
									<div class="span8 tajuk relative">
                                        <div class="control-ticker">
                                            <a class="btn custom mini icn-only news-up">
                                                <i class="icon-angle-up"></i>
                                            </a>
                                            <br />
                                            <a class="btn custom mini icn-only news-down">
                                                <i class="icon-angle-down"></i>
                                            </a>
                                        </div>
                                        <div class="sticker">
                                            <ul>
                                                <?php $berita = new WP_Query(array('post_type'=>'berita', 'page_per_posts' => 4, 'meta_key' => 'views', 'orderby' => 'meta_value'));?>
                                                <?php while ( $berita->have_posts( ) ) : $berita->the_post(); ?>
                                                <?php //echo "<pre>"; print_r($berita); exit; ?>
                                                <li>
                                                    <div class="span4 gambar-tajuk">
                                                        <?php if ( has_post_thumbnail($post->ID) ) :?>
                                                        <!-- check if the post has a Post Thumbnail assigned to it. -->
                                                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                                        <!--<img src="<?php //bloginfo('template_directory')?>/img/image2.jpg" alt="" />-->
                                                        <?php the_post_thumbnail(); ?>
                                                        <?php endif; ?> 
                                                    </div>
                                                    <div class="span8 konten-tajuk">
                                                        <a href="<?php the_permalink($post->ID); ?>"><h3><?php echo $jmlh; the_title(); ?></h3></a>
                                                        <ul class="unstyled inline">
                											<li><i class="icon-calendar"></i> <a href="#"> <?php the_date(); ?></a></li>
                											<li><i class="icon-comments"></i> <a href="<?php comments_link(); ?>"> <?php comments_number('0 comment','1 comment','% comments'); ?></a></li>
                										</ul>
    										            <p class="margin-top-10"><?php the_excerpt('selengkapnya', TRUE);?></p>
                                                    </div>
                                                </li>
                                                <?php
                                                    endwhile;
                                                ?>
                                            </ul>
                                        </div>  
									</div>
									<div class="span4 daftar-berita relative">
                                        <h3>BERITA TERBARU</h3>
										<ul class="unstyled">
                                            <li><a href="#">Berita</a></li>
                                            <li><a href="#">Berita</a></li>
                                            <li><a href="#">Berita</a></li>
                                            <li><a href="#">Berita</a></li>
                                        </ul>
                                        <a class="button plus btn green" href="#"><i class="icon-plus"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="functionality">
							<div class="container">
								<div class="row-fluid">
									<div class="span4">
										<div class="layanan">
                                            <div class="row-fluid">
                                                <div class="span12 polling relative">
                                                    <!--<h3>POLLING</h3>
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos <a href="#">ellentesque la vehi</a> dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                    <a class="button plus btn green" href="#"><i class="icon-plus"></i></a>-->
                                                    <?php if (function_exists('vote_poll') && !in_pollarchive()): ?>
                                                        <h3>POLING</h3>
                                                        <p><?php get_poll();?></p>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="span12 layanan-informasi-publik relative">
                                                    <h3>LAYANAN INFORMASI PUBLIK</h3>
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos <a href="#">ellentesque la vehi</a> dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique. </p>
                                                    <a class="button plus btn green" href="http://www.pu.go.id/layananinformasi/service"><i class="icon-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
									</div>
                                    <div class="span4">
                                        <div class="row-fluid">
                                            <div class="span12 proyek-strategis relative">
        										<h3>PROYEK STRATEGIS</h3>
        										<ul class="unstyled">
                                                    <?php $proyek = new WP_Query(array('post_type'=>'pekerjaan', 'page_per_posts' => 4)); ?>
                                                    <?php while ( $proyek->have_posts( ) ) : $proyek->the_post(); ?>
                                                    <?php //echo "<pre>"; print_r($proyek); exit; ?>
        											<li>
                                                        <a href="<?php the_permalink($post->ID); ?>"><?php echo strtoupper($post->post_title); ?></a>
                                                    </li>
        											<?php endwhile; ?>
        										</ul>
                                                <a class="button plus btn green" href="#"><i class="icon-plus"></i></a>
        									</div>
                                            <div class="span12 pelaporan relative">
        										<!--<h3>PELAPORAN BENCANA</h3>-->
                                                <a href="http://bencana.pdsda.net"><img src="<?php bloginfo('template_directory')?>/img/pelaporan.jpg" alt=""></a>
        									</div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="row-fluid">
                                            <div class="span12 kalender">
        										<h3>KALENDER</h3>
                                                <!--<div id="full-clndr" class="clearfix">
                                                  <script type="text/template" id="full-clndr-template">
                                                    <div class="clndr-grid">
                                                      <div class="days-of-the-week clearfix">
                                                        <% _.each(daysOfTheWeek, function(day) { %>
                                                          <div class="header-day"><%= day %></div>
                                                        <% }); %>
                                                      </div>
                                                      <div class="days clearfix">
                                                        <% _.each(days, function(day) { %>
                                                          <div class="<%= day.classes %>" id="<%= day.id %>"><span class="day-number"><%= day.day %></span></div>
                                                        <% }); %>
                                                      </div>
                                                    </div>
                                                    <div class="event-listing">
                                                      <div class="event-listing-title">EVENTS THIS MONTH</div>
                                                      <% _.each(eventsThisMonth, function(event) { %>
                                                          <div class="event-item">
                                                            <div class="event-item-name"><%= event.title %></div>
                                                            <div class="event-item-location"><%= event.location %></div>
                                                          </div>
                                                        <% }); %>
                                                    </div>
                                                  </script>
                                                </div>-->
                                                <div id="mini-clndr">
                                                  <script id="mini-clndr-template" type="text/template">
                                                    <div class="controls">
                                                      <div class="clndr-previous-button">&lsaquo;</div><div class="month"><%= month %></div><div class="clndr-next-button">&rsaquo;</div>
                                                    </div>
                                        
                                                    <div class="days-container">
                                                      <div class="days">
                                                        <div class="headers">
                                                          <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
                                                        </div>
                                                        <% _.each(days, function(day) { %><div class="<%= day.classes %>" id="<%= day.id %>"><%= day.day %></div><% }); %>
                                                      </div>
                                                      <div class="events">
                                                        <div class="headers">
                                                          <div class="x-button">x</div>
                                                          <div class="event-header">EVENTS</div>
                                                        </div>
                                                        <div class="events-list">
                                                          <% _.each(eventsThisMonth, function(event) { %>
                                                            <div class="event">
                                                              <a href="<%= event.url %>"><%= moment(event.start).format('MMMM Do') %>: <%= event.title %></a>
                                                            </div>
                                                          <% }); %>
                                                        </div>
                                                      </div>
                                                    </div>
                                        
                                                  </script>
                                                </div>
        									</div>
                                            <div class="span12 perijinan">
                                                <a href="http://sda.pu.go.id/bpsda/ppsda"><img src="<?php bloginfo('template_directory')?>/img/perijinan.jpg" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
                        <!--<div class="afiliasi">
                            <div class="container">
                                <div class="row-fluid">
                                     Jssor Slider Begin 
                                     You can move inline styles to css file or css block. 
                                    <div class="span12">
                                        <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 1900px; height: 200px; overflow: hidden;">
                                
                                           Loading Screen 
                                            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                                                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                                    background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                                                </div>
                                                <div style="position: absolute; display: block; background: url(../wordpress/wp-content/themes/bwskal/img/loading.gif) no-repeat center center;
                                                    top: 0px; left: 0px;width: 100%;height:100%;">
                                                </div>
                                            </div>
                                    
                                            Slides Container
                                            <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1900px; height: 200px; overflow: hidden;">
                                                <div><img u="image" src="<?php bloginfo('template_directory')?>/img/afiliasi/greenpeacelogo.jpg" /></div>
                                                <div><img u="image" src="<?php bloginfo('template_directory')?>/img/afiliasi/logo_lipi.jpg" /></div>
                                                <div><img u="image" src="<?php bloginfo('template_directory')?>/img/afiliasi/Pertamina_logo.jpg" /></div>
                                                <div><img u="image" src="<?php bloginfo('template_directory')?>/img/afiliasi/waskita-karya.jpg" /></div>
                                                <div><img u="image" src="<?php bloginfo('template_directory')?>/img/afiliasi/WWFsmall.jpg" /></div>
                                            </div>
                                            
                                            Bullet Navigator Skin Begin
                                            <style>
                                                /* jssor slider bullet navigator skin 03 css */
                                                /*
                                                .jssorb03 div           (normal)
                                                .jssorb03 div:hover     (normal mouseover)
                                                .jssorb03 .av           (active)
                                                .jssorb03 .av:hover     (active mouseover)
                                                .jssorb03 .dn           (mousedown)
                                                */
                                                .jssorb03 div, .jssorb03 div:hover, .jssorb03 .av
                                                {
                                                    background: url(../img/b03.png) no-repeat;
                                                    overflow:hidden;
                                                    cursor: pointer;
                                                }
                                                .jssorb03 div { background-position: -5px -4px; }
                                                .jssorb03 div:hover, .jssorb03 .av:hover { background-position: -35px -4px; }
                                                .jssorb03 .av { background-position: -65px -4px; }
                                                .jssorb03 .dn, .jssorb03 .dn:hover { background-position: -95px -4px; }
                                            </style>
                                            bullet navigator container
                                            <div u="navigator" class="jssorb03" style="position: absolute; bottom: 4px; right: 6px;">
                                                bullet navigator item prototype
                                                <div u="prototype" style="position: absolute; width: 21px; height: 21px; text-align:center; line-height:21px; color:white; font-size:12px;"><NumberTemplate></NumberTemplate></div>
                                            </div>
                                            Bullet Navigator Skin End
                                            
                                            Arrow Navigator Skin Begin
                                            <style>
                                                /* jssor slider arrow navigator skin 03 css */
                                                /*
                                                .jssora03l              (normal)
                                                .jssora03r              (normal)
                                                .jssora03l:hover        (normal mouseover)
                                                .jssora03r:hover        (normal mouseover)
                                                .jssora03ldn            (mousedown)
                                                .jssora03rdn            (mousedown)
                                                */
                                                .jssora03l, .jssora03r, .jssora03ldn, .jssora03rdn
                                                {
                                                	position: absolute;
                                                	cursor: pointer;
                                                	display: block;
                                                    background: url(../wordpress/wp-content/themes/bwskal/img/a02.png) no-repeat;
                                                    overflow:hidden;
                                                }
                                                .jssora03l { background-position: -3px -33px; }
                                                .jssora03r { background-position: -63px -33px; }
                                                .jssora03l:hover { background-position: -123px -33px; }
                                                .jssora03r:hover { background-position: -183px -33px; }
                                                .jssora03ldn { background-position: -243px -33px; }
                                                .jssora03rdn { background-position: -303px -33px; }
                                            </style>
                                            Arrow Left
                                            <span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
                                            </span>
                                            Arrow Right
                                            <span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 123px; right: 8px">
                                            </span>
                                            Arrow Navigator Skin End
                                            <a style="display: none" href="http://www.jssor.com">slideshow</a>
                                        </div>
                                    </div>
                                </div>
                            </div>          
                        </div>-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
        <div class="link-page">
            <ul class="unstyled">
                <li class="link-berita"><a href="<?php bloginfo('url')?>/?page_id=10">BERITA</a></li>
                <li class="link-forum"><a href="#">FORUM</a></li>
            </ul>
        </div>
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<?php include('footer.php'); ?>
