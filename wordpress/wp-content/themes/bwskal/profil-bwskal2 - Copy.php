<?php
    /*Template Name: profil_bwskal2
    Theme Name : bwskal2*/
?>
<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="profil">
							<div class="container">
                                <div class="row-fluid page-title">
                                    <h3>PROFIL BALAI WILAYAH SUNGAI KALIMANTAN II</h3>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3 sidebar-content">
                                        <ul class="ver-inline-menu tabbable margin-bottom-25">
                							<li class="active">
                								<a href="#tab_1" data-toggle="tab" id="informasi_bwskal2">
                								<i class="icon-briefcase"></i> 
                								Informasi Umum
                								</a> 
                								<span class="after"></span>                                    
                							</li>
                							<li><a href="#tab_2" data-toggle="tab" id="struktur_bwskal2"><i class="icon-leaf"></i> Struktur</a></li>
                							<li><a href="#tab_3" data-toggle="tab" id="fungsi_bwskal2"><i class="icon-info-sign"></i> Fungsi</a></li>
                							<li><a href="#tab_4" data-toggle="tab" id="wilayah_kerja_bwskal2"><i class="icon-tint"></i> Wilayah Kerja</a></li>
                						</ul>
                                    </div>
                                    <div class="span9 tab-content profil-content">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <h3>INFORMASI UMUM</h3>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.
                                                The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tab_2">
                                                <h3>STRUKTUR BALAI WILAYAH SUNGAI KALIMANTAN 2</h3>
                                                <p>shahads</p>
                                            </div>
                                            <div class="tab-pane" id="tab_3">
                                                <h3>FUNGSI</h3>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_4">
                                                <h3>WILAYAH KERJA</h3>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_5">
                                                <p>cvb</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>     
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<?php include("footer.php") ?>