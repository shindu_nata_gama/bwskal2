<?php
    /*Template Name: pekerjaan
    Theme Name : bwskal2*/
?>
<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="news-list">
							<div class="container">
                                <div class="row-fluid page-title two-title">
                                    <h2>PAKET PEKERJAAN</h2>
                                </div>
                                <ul style="display: inline;margin-left:auto;margin-right:auto;>
                                    <?php $query = new WP_Query(array('post_type'=>'pekerjaan', 'paged' => get_query_var('paged')));?>
                                    <?php if ( $query->have_posts() ) : ?>
                                    <?php while ( $query->have_posts( ) ) : $query->the_post(); ?>
                                    <li style="list-style:none;">
                                       <div class="span3 potensi-item" style="margin-right: 20px;">
                                                <?php if ( has_post_thumbnail($post->ID) ) :?>
                                                 <!-- check if the post has a Post Thumbnail assigned to it. -->
                                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                                <div class="ch-item" style="background-image: url(<?php echo $image[0]; ?>);">     
                                                <?php endif; ?>                                        
                    							<div class="ch-info">
                    								<a style="color:white" href="<?php the_permalink($post->ID); ?> "><h3><?php the_title(); ?></h3></a>
                    							</div>
                    						</div>
                                        </div>
                                    </li>
                                    <?php endwhile; ?>
                                    <?php else : ?>
                                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                                    <?php endif; ?>
                                </ul> 
							</div>
						</div>
						<br />
                        <div class="halaman">
                            <div class="container">
                                <div class="row-fluid">
                                    <div class="paginate">
                                        <span class="current">1</span>
                                        <a rel="noindex, follow" href="/awards-of-the-month/?page=2">2</a>
                                        <a rel="noindex, follow" href="/awards-of-the-month/?page=3">3</a>
                                        <a rel="noindex, follow" href="/awards-of-the-month/?page=4">4</a>
                                        <a rel="noindex, follow" href="/awards-of-the-month/?page=5">5</a>
                                        <a class="bt-pag next" rel="noindex, follow" href="/awards-of-the-month/?page=2">></a>
                                    </div>
                                </div>
                            </div>          
                        </div>  
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<script>
var _SlideshowTransitions = [
            //Fade in L
                {$Duration: 1200, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }];
            //image slider potensi
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1

                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 800,                                //Specifies default duration (swipe) for slide in milliseconds

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                },

                $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 10,                             //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 360                          //[Optional] The offset position to park thumbnail
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$SetScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
</script>
<?php include("footer.php") ?>