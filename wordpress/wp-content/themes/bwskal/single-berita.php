<?php
    /*Template Name: detail_berita
    Theme Name : bwskal2*/
    global $post;
    if ( !is_user_logged_in() ){
         $currentViews = ( (int)get_post_meta( $post->ID, 'views', true ) + 1 );
         update_post_meta( $post->ID, 'views', $currentViews );
    }
?>

<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12 page-berita">
                        <div class="container">
                            <div class="span9 left-content">
                                <div class="news-detail">
                                    <div class="row-fluid">
                                    <?php while ( have_posts() ) : the_post(); ?>
                                         <div class="span12 news-detail-content">
                                            <h1><?php the_title(); ?></h1>
            								<div class="row-fluid">
            									<div class="span6">
                                                    <?php the_tags('<ul class="unstyled inline blog-tags"><i class="icon-tags" style="color:#78cff8"></i><li>','','</li></ul>'); ?>
                                                </div>
                                                <div class="span6 blog-tag-data-inner">
                                                    <ul class="unstyled inline">
                                                        <li><i class="icon-calendar"></i> <?php the_date(); ?></li>
                       									<li><i class="icon-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number('0 comment','1 comment','% comments'); ?></a></li>
                                                    </ul>
            									</div>
                                            </div>
                                            <p><?php the_content(); ?></p>
              							</div>
                                     </div>
                                     <?php endwhile; ?>
                                 </div>
                                 <?php comments_template('',TRUE); ?>
					           </div>
                                <div class="span3 right-content">
                                    <div class="berita-lain">
                                        <h2>Berita Lain</h2>
                                        <ul class="unstyled">
                                            <li>
                                                <a  href="http://localhost/bwskal2/wordpress/?p=87"><h5><strong>Pemberdayaan Masyarakat Tepi Sungai</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.lit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                            <li>
                                                <a  href="http://localhost/bwskal2/wordpress/?p=85"><h5><strong>Pemeliharaan Waduk</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                            <li>
                                                <a href="http://localhost/bwskal2/wordpress/?p=82"><h5><strong>Pencemaran Air</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
</div>
    <!-- END CONTAINER -->
<?php include("footer.php") ?>