<?php

if ( post_password_required() )
	return;
?>
<!--<div class="media">
    	<h3>Komentar</h3>
    	<a href="#" class="pull-left">
    	<img alt="" src="assets/img/blog/9.jpg" class="media-object">
    	</a>
    	<div class="media-body">
    		<h4 class="media-heading">Media heading <span>5 hours ago / <a href="#">Balas</a></span></h4>
    		<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    		<hr>
    		Nested media object
    		<div class="media">
    			<a href="#" class="pull-left">
    			<img alt="" src="assets/img/blog/5.jpg" class="media-object">
    			</a>
    			<div class="media-body">
    				<h4 class="media-heading">Media heading <span>17 hours ago / <a href="#">Balas</a></span></h4>
    				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    			</div>
    		</div>
    		end media
    		<hr>
    		<div class="media">
    			<a href="#" class="pull-left">
    			<img alt="" src="assets/img/blog/7.jpg" class="media-object">
    			</a>
    			<div class="media-body">
    				<h4 class="media-heading">Media heading <span>2 days ago / <a href="#">Balas</a></span></h4>
    				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    			</div>
    		</div>
    		end media
    	</div>
    </div>
    <hr>
    <div class="post-comment">
    	<h3>Tinggalkan Komentar</h3>
    	<form>
    		<label>Nama</label>
    		<input type="text" class="span7 m-wrap">
    		<label>Email <span class="color-red">*</span></label>
    		<input type="text" class="span7 m-wrap">
    		<label>Pesan</label>
    		<textarea class="span10 m-wrap" rows="8"></textarea>
    		<p><button class="btn blue" type="submit">Kirim Komentar</button></p>
    	</form>
    </div>-->
<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h3 class="comments-title">
			<?php
				printf( _n( '1 komentar', '%1$s komentar', get_comments_number(), 'bwskal' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h3>

		<ol class="commentlist">
			<?php wp_list_comments( array( 'callback' => 'bwskal_comment', 'style' => 'ol' ) ); ?>
		</ol><!-- .commentlist -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php _e( 'Comment navigation', 'bwskal' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'bwskal' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'bwskal' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a note.
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Comments are closed.' , 'bwskal' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>
    <hr />
	<?php 
            $args = array(
                          'id_form'           => 'commentform',
                          'id_submit'         => 'submit',
                          'title_reply'       => __( 'Komentar Pembaca' ),
                          'title_reply_to'    => __( 'Komentar untuk %s' ),
                          'cancel_reply_link' => __( 'Batal' ),
                          'label_submit'      => __( 'Kirim' ),
                        
                          'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
                            '</label><textarea id="comment" name="comment" class="span10 m-wrap" required rows="8" aria-required="true">' .
                            '</textarea></p>',
                        
                          'must_log_in' => '<p class="must-log-in">' .
                            sprintf(
                              __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
                              wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
                            ) . '</p>',
                        
                          'logged_in_as' => '<p class="logged-in-as">' .
                            sprintf(
                            __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
                              admin_url( 'profile.php' ),
                              $user_identity,
                              wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
                            ) . '</p>',
                        
                          'comment_notes_before' => '<p class="comment-notes">' .
                            __( 'Email Anda tidak akan dipublikasikan.' ) . ( $req ? $required_text : '' ) .
                            '</p>',
                        
                          'comment_notes_after' => '',
                        
                          'fields' => apply_filters( 'comment_form_default_fields', array(
                        
                            'author' =>
                              '<p class="comment-form-author">' .
                              '<label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
                              '<input id="author" class="span7 m-wrap" name="author" type="text" required value="' . esc_attr( $commenter['comment_author'] ) .
                              '" size="30"' . $aria_req . ' /></p>',
                        
                            'email' =>
                              '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
                              '<input id="email" class="span7 m-wrap" name="email" type="text" required value="' . esc_attr(  $commenter['comment_author_email'] ) .
                              '" size="30"' . $aria_req . ' /></p>'
                            )
                          ),
                        );
                        
                        comment_form($args);                                                
         ?>

</div><!-- #comments .comments-area -->