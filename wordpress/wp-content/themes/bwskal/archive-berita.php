<?php
    /*Template Name: daftar_berita
    Theme Name : bwskal2*/
?>
<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="news-list">
							<div class="container">
                                <div class="row-fluid page-title">
                                    <h3>DAFTAR BERITA</h3>
                                </div>
                                <div class="coba" style="margin-top: -20px;">
                                    <ul style="display: inline;">
                                        <?php $query = new WP_Query(array('post_type'=>'berita', 'paged' => get_query_var('paged')));?>
                                        <?php if ( $query->have_posts() ) : ?>
                                        <?php while ( $query->have_posts( ) ) : $query->the_post(); ?>
                                        <li style="list-style:none;">
                                            <div class="span3 news-item relative shadow" style="height:530px; margin-right: 22.51px;">
                                                 <div class="news-item-pic" style="text-align: center;margin-top:15px;">
                                                    <?php 
                                                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                                    	the_post_thumbnail('berita-thumb');
                                                    } 
                                                    ?>
                                                    <!--<img src="<?php bloginfo('template_directory')?>/img/image2.jpg" alt=""/>-->
                                                 </div>
                                                 <div class="news-item-content">
                                                      <h4><?php the_title(); ?></h4>
                                                      <div class="news-data">
                                                          <ul class="unstyled inline">
                									           <li><i class="icon-calendar"></i> <?php the_date(); ?></li>
                									           <li><i class="icon-comments"></i><a href="<?php comments_link(); ?>"> <?php comments_number('0 comment','1 comment','% comments'); ?></a></li>
                                                          </ul>
                                                      </div>
                                                      <p><?php the_excerpt('selengkapnya',true); ?></p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                    </ul>
                                </div>
                                <div class="row-fluid">
                                    <div class="halaman">
                                        <div class="container">
                                            <?php wp_pagenavi(array('query' => $query)); ?>
                                            <?php wp_reset_postdata(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php else : ?>
                                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                                <?php endif; ?>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
    <!-- END CONTAINER -->
<?php include("footer.php") ?>