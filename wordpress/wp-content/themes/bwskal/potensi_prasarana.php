<?php
    /*Template Name: potensi_prasarana
    Theme Name : bwskal2*/
?>
<?php include("header.php") ;
        global $post;
?>
<script src="wp-content/themes/bwskal/js/jssor.core.js" type="text/javascript" ></script>
    <script src="wp-content/themes/bwskal/js/jssor.utils.js" type="text/javascript" ></script>
    <script src="wp-content/themes/bwskal/js/jssor.slider.min.js" type="text/javascript" ></script>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12 page-potensi">
                        <div class="container">
                            <div class="span9 left-content">
                                <div class="potensi-detail">
                                    <div class="row-fluid">
                                         <div class="span12 potensi-detail-content">
                                            <?php $query = new WP_Query(array('page_id'=>$post->ID)); ?>
                                            <?php while ( $query->have_posts( ) ) : $query->the_post(); ?>
                                            <h1><?php $judul = explode('-', $post->post_title); echo strtoupper($judul[1]); ?></h1>
					                        <!-- Jssor Slider Begin 
                                             You can move inline styles to css file or css block. 
                                            <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 800px;margin-right:auto;margin-left:auto;
                                                height: 456px; background: #191919; overflow: hidden;">
                                        
                                                 Loading Screen 
                                                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                                                    <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                                        background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
                                                    </div>
                                                    <div style="position: absolute; display: block; background: url(<?php bloginfo('template_directory')?>/img/loading.gif) no-repeat center center;
                                                        top: 0px; left: 0px;width: 100%;height:100%;">
                                                    </div>
                                                </div>
                                        
                                                 Slides Container 
                                                <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 800px; height: 356px; overflow: hidden;">
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/01.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-01.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/02.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-02.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/03.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-03.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/04.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-04.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/05.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-05.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/06.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-06.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/07.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-07.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/08.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-08.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/09.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-09.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/10.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-10.jpg" />
                                                    </div>
                                                    
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/11.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-11.jpg" />
                                                    </div>
                                                    <div>
                                                        <img u="image" src="<?php bloginfo('template_directory')?>/img/alila/12.jpg" />
                                                        <img u="thumb" src="<?php bloginfo('template_directory')?>/img/alila/thumb-12.jpg" />
                                                    </div>
                                                </div>
                                                
                                                 Arrow Navigator Skin Begin 
                                                <style>
                                                    /* jssor slider arrow navigator skin 05 css */
                                                    /*
                                                    .jssora05l              (normal)
                                                    .jssora05r              (normal)
                                                    .jssora05l:hover        (normal mouseover)
                                                    .jssora05r:hover        (normal mouseover)
                                                    .jssora05ldn            (mousedown)
                                                    .jssora05rdn            (mousedown)
                                                    */
                                                    .jssora05l, .jssora05r, .jssora05ldn, .jssora05rdn
                                                    {
                                                    	position: absolute;
                                                    	cursor: pointer;
                                                    	display: block;
                                                        background: url(<?php bloginfo('template_directory')?>/img/a17.png) no-repeat;
                                                        overflow:hidden;
                                                    }
                                                    .jssora05l { background-position: -10px -40px; }
                                                    .jssora05r { background-position: -70px -40px; }
                                                    .jssora05l:hover { background-position: -130px -40px; }
                                                    .jssora05r:hover { background-position: -190px -40px; }
                                                    .jssora05ldn { background-position: -250px -40px; }
                                                    .jssora05rdn { background-position: -310px -40px; }
                                                </style>
                                                 Arrow Left 
                                                <span u="arrowleft" class="jssora05l" style="width: 40px; height: 40px; top: 158px; left: 8px;">
                                                </span>
                                                 Arrow Right 
                                                <span u="arrowright" class="jssora05r" style="width: 40px; height: 40px; top: 158px; right: 8px">
                                                </span>
                                                 Arrow Navigator Skin End 
                                                
                                                 Thumbnail Navigator Skin Begin 
                                                <div u="thumbnavigator" class="jssort01" style="position: absolute; width: 800px; height: 100px; left:0px; bottom: 0px;">
                                                     Thumbnail Item Skin Begin 
                                                    <style>
                                                        /* jssor slider thumbnail navigator skin 01 css */
                                                        /*
                                                        .jssort01 .p           (normal)
                                                        .jssort01 .p:hover     (normal mouseover)
                                                        .jssort01 .pav           (active)
                                                        .jssort01 .pav:hover     (active mouseover)
                                                        .jssort01 .pdn           (mousedown)
                                                        */
                                                        .jssort01 .w {
                                                            position: absolute;
                                                            top: 0px;
                                                            left: 0px;
                                                            width: 100%;
                                                            height: 100%;
                                                        }
                                        
                                                        .jssort01 .c {
                                                            position: absolute;
                                                            top: 0px;
                                                            left: 0px;
                                                            width: 68px;
                                                            height: 68px;
                                                            border: #000 2px solid;
                                                        }
                                        
                                                        .jssort01 .p:hover .c, .jssort01 .pav:hover .c, .jssort01 .pav .c {
                                                            background: url(<?php bloginfo('template_directory')?>/img/t01.png) center center;
                                                            border-width: 0px;
                                                            top: 2px;
                                                            left: 2px;
                                                            width: 68px;
                                                            height: 68px;
                                                        }
                                        
                                                        .jssort01 .p:hover .c, .jssort01 .pav:hover .c {
                                                            top: 0px;
                                                            left: 0px;
                                                            width: 70px;
                                                            height: 70px;
                                                            border: #fff 1px solid;
                                                        }
                                                    </style>
                                                    <div u="slides" style="cursor: move;">
                                                        <div u="prototype" class="p" style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
                                                            <div class=w><thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate></div>
                                                            <div class=c>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     Thumbnail Item Skin End 
                                                </div>
                                                 Thumbnail Navigator Skin End 
                                                <a style="display: none" href="http://www.jssor.com">slideshow</a>
                                            </div>
                                             Jssor Slider End -->
                							<p><?php the_content() ; ?></p>
                			                 <?php endwhile; ?>
                                         </div>
                                     </div>
                                 </div>
                            </div>
                                <div class="span3 right-content">
                                    <div class="potensi-lain">
                                        <h2>Potensi Lain</h2>
                                        <ul class="unstyled">
                                            <li>
                                                <div class="potensi-lain-item" style="background-image: url(<?php bloginfo('template_directory')?>/img/image2.jpg);">
                        							<div class="potensi-lain-info">
                        								<h3>Danau</h3>
                        								<p>Banjarmasin <a href="http://localhost/bwskal2/wordpress/?p=177">Detail</a></p>
                        							</div>
                        						</div>
                                            </li>
                                            <li>
                                                <div class="potensi-lain-item" style="background-image: url(<?php bloginfo('template_directory')?>/img/image2.jpg);">
                        							<div class="potensi-lain-info">
                        								<h3>Irigasi</h3>
                        								<p>Banjarmasin <a href="http://localhost/bwskal2/wordpress/?p=175 ?">Detail</a></p>
                        							</div>
                        						</div>
                                            </li>
                                            <li>
                                                <div class="potensi-lain-item" style="background-image: url(<?php bloginfo('template_directory')?>/img/image2.jpg);">
                        							<div class="potensi-lain-info">
                        								<h3>Rawa</h3>
                        								<p>Banjarmasin <a href="http://localhost/bwskal2/wordpress/potensi-dan-prasarana/potensidanprasarana-rawa/ ?">Detail</a></p>
                        							</div>
                        						</div>
                                            </li>
                                            <li>
                                                <div class="potensi-lain-item" style="background-image: url(<?php bloginfo('template_directory')?>/img/image2.jpg);">
                        							<div class="potensi-lain-info">
                        								<h3>Pantai</h3>
                        								<p>Banjarmasin <a href="http://localhost/bwskal2/wordpress/potensi-dan-prasarana/potensidanprasarana-pantai ?">Detail</a></p>
                        							</div>
                        						</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
</div>
    <!-- END CONTAINER -->
    <script>
    var _SlideshowTransitions = [
            //Fade in L
                {$Duration: 1200, $During: { $Left: [0.3, 0.7] }, $FlyDirection: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $ScaleHorizontal: 0.3, $Opacity: 2 }];
            //image slider potensi
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1

                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 800,                                //Specifies default duration (swipe) for slide in milliseconds

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                },

                $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 10,                             //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 360                          //[Optional] The offset position to park thumbnail
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$SetScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
    </script>
<?php include("footer.php") ?>