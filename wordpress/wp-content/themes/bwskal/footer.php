<!-- BEGIN FOOTER1 -->
	<div class="footer">
    		<div class="container">
                <div class="footer-menu">
                    <div class="row-fluid">
                        <div class="span3 alamat">
                            <h4>Alamat</h4>
                	        <address>
                			     <strong>Balai Wilayah Sungai Kalimantan II</strong><br>
                			     Jalan Tambun Bungai No.25<br>
                			     Kuala Kapuas, Kalimantan Tengah 731514<br>
                			     <abbr title="Telepon">Telp :</abbr> (0513) 22085<br />
                                 <abbr title="Fax">Fax :</abbr> (0513) 22086
                			</address>
                			<address>
                				<strong>Email</strong><br>
                				<a href="mailto:#">bwskal2@gmail.com</a>
                			</address>
                        </div>
                        <div class="span3 offset3 link">
                            <h4>Link Terkait</h4>
                                <?php
                                    $link = new WP_Query(array('category_name' => 'link-terkait'));
                                    //echo '<pre>';print_r($link);exit;    
                                ?>
                                <?php while ( $link->have_posts( ) ) : $link->the_post(); ?>
                                <?php the_content(); ?>
                                <?php endwhile; ?>
                        </div>
                        <div class="span3 counter">
                            <h4>Pengunjung</h4>
                                
                        </div>
                    </div>
                </div>
                <div class="id">
                    <div class="row-fluid">
                        <div class="span12">
                			<div class="footer-inner">
                				2014 &copy; BWSKAL2.
                			</div>
                			<div class="footer-tools">
                				<span class="go-top">
                				<i class="icon-angle-up"></i>
                				</span>
                			</div>
                        </div>
                    </div>
                </div>
    		</div>        
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>	     
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->        
    
    
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/less.js" type="text/javascript"></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/underscore.js" type="text/javascript"></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/moment.js" type="text/javascript"></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/clndr.min.js" type="text/javascript"></script>    
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.uniform.min.js" type="text/javascript" ></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/bootstrap-carousel.min.js" type="text/javascript" ></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.easy-ticker.min.js" type="text/javascript" ></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/jquery.easing.min.js" type="text/javascript" ></script>
    <script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/modernizr.custom.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<script src="<?php bloginfo('url')?>/wp-content/themes/bwskal/js/app.js"></script> 
	<script>
		jQuery(document).ready(function() {    
		   App.init();
		   jQuery('#promo_carousel').carousel({
		      interval: 10000,
		      pause: 'hover'
		   });
           
           //CLNDR.js
            var currentMonth = moment().format('YYYY-MM');
              var nextMonth    = moment().add('month', 1).format('YYYY-MM');
              var events = [
                { start: '2014-08-10', end: '2014-08-10', title: 'Persian Kitten Auction', location: 'Center for Beautiful Cats' },
                { start: '2014-08-19', end: '2014-08-19', title: 'Cat Frisbee', location: 'Jefferson Park' },
                { start: '2014-08-23', end: '2014-08-23', title: 'Kitten Demonstration', location: 'Center for Beautiful Cats' },
                { start: '2014-09-7', end: '2014-09-7',    title: 'Small Cat Photo Session', location: 'Center for Cat Photography' },
                { start: '2014-09-7', end: '2014-09-14',    title: 'Hari Ganteng Sedunia', location: 'PTIIK-UB' }
              ];
            
              $('#full-clndr').clndr({
                template: $('#full-clndr-template').html(),
                events: events
              });
              
              clndr = $('#mini-clndr').clndr({
                template: $('#mini-clndr-template').html(),
                events: events,
                multiDayEvents: {
                    startDate: 'start',
                    endDate: 'end'
                  },
                clickEvents: {
                  click: function(target) {
                    if(target.events.length) {
                      var daysContainer = $('#mini-clndr').find('.days-container');
                      daysContainer.toggleClass('show-events', true);
                      $('#mini-clndr').find('.x-button').click( function() {
                        daysContainer.toggleClass('show-events', false);
                      });
                    }
                  }
                },
                adjacentDaysChangeMonth: true
              });
              
            //tabs untuk profil
            jQuery('#tabs').tabs();
            
            //newsticker
            jQuery('.ticker').easyTicker({
            	direction: 'up',
            	easing: 'swing',
            	speed: 'slow',
            	interval: 1800,
            	height: 'auto',
            	visible: 1,
            	mousePause: 1,
            	controls: {
            		up: '',
            		down: '',
            		toggle: '',
            		playText: '',
            		stopText: ''
            	}
            });
            
            //tajuk
            $('.sticker').easyTicker({
        		direction: 'down',
                interval: 5000,
                speed: 'slow',
                visible: 1,
                controls: {
            		up: '.news-up',
            		down: '.news-down',
            		toggle: '',
            		playText: '',
            		stopText: ''
            	}
        	});
            

		});
	</script>
    <?php wp_footer();?>
</body>
<!-- END BODY -->
</html>