<?php
//Register my menu
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
    global $post;
	return '<a class="moretag button plus btn green" href="'. get_permalink($post->ID) . '"> Baca Selengkapnya...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function get_news_excerpt(){
	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 30);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'... <a href="'.$permalink.'">Continue reading ?</a>';
	return $excerpt;
}
//create tipe artikel berita a.k.a post_type
function berita_post_type_init(){
    $labels = array(
    'name'               => _x( 'Berita', 'post type general name' ),
    'singular_name'      => _x( 'Berita', 'post type singular name' ),
    'add_new'            => _x( 'Tambah Baru', 'berita' ),
    'add_new_item'       => __( 'Tambah Berita Baru' ),
    'edit_item'          => __( 'Edit Berita' ),
    'new_item'           => __( 'Berita Baru' ),
    'all_items'          => __( 'Semua Berita' ),
    'view_item'          => __( 'Lihat Berita' ),
    'search_items'       => __( 'Cari Berita' ),
    'not_found'          => __( 'Berita tidak ditemukan' ),
    'not_found_in_trash' => __( 'Berita tidak ditemukan di pembuangan' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Berita'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our news and news specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'trackbacks', 'page-attributes', 'custom-fields', 'author' ),
    'has_archive'   => true,
    'menu_icon'     => 'dashicons-media-text',
    'show_ui'       => true,
    'query_var'     => true,
  );
  register_post_type( 'berita', $args ); 
}
add_action( 'init', 'berita_post_type_init' );

function pekerjaan_post_type_init(){
    $labels = array(
    'name'               => _x( 'Pekerjaan', 'post type general name' ),
    'singular_name'      => _x( 'Pekerjaan', 'post type singular name' ),
    'add_new'            => _x( 'Tambah Baru', 'Pekerjaan' ),
    'add_new_item'       => __( 'Tambah Pekerjaan Baru' ),
    'edit_item'          => __( 'Edit Pekerjaan' ),
    'new_item'           => __( 'Pekerjaan Baru' ),
    'all_items'          => __( 'Semua Pekerjaan' ),
    'view_item'          => __( 'Lihat Pekerjaan' ),
    'search_items'       => __( 'Cari Pekerjaan' ),
    'not_found'          => __( 'Pekerjaan tidak ditemukan' ),
    'not_found_in_trash' => __( 'Pekerjaan tidak ditemukan di pembuangan' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Pekerjaan'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our news and news specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'trackbacks', 'page-attributes', 'custom-fields', 'author' ),
    'has_archive'   => true,
    'menu_icon'     => 'dashicons-video-alt',
    'show_ui'       => true,
    'query_var'     => true,


  );
  register_post_type( 'pekerjaan', $args ); 
}
add_action( 'init', 'pekerjaan_post_type_init' );

/**
*
* Comments
*
*/
if ( ! function_exists( 'bwskal_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own experiment_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function bwskal_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class('media'); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'bwskal' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'bwskal' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class('media'); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard"">
				<?php
                    echo "<a class='pull-left' style='margin-right: 10px';>";
					echo get_avatar( $comment, 44 );
                    echo "</a>";
				?>
			</header><!-- .comment-meta -->
            
            <div class="media-body">
                <?php
                printf( '<h4 class="media-heading">%1$s',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span style="float:right;">' . __( 'Post author', 'bwskal' ) . '</span>' : ''
					);
                    
                printf( '<span><time datetime="%2$s">%3$s</time> / ',
    						esc_url( get_comment_link( $comment->comment_ID ) ),
    						get_comment_time( 'c' ),
    						/* translators: 1: date, 2: time */
    						sprintf( __( '%1$s at %2$s', 'bwskal' ), get_comment_date(), get_comment_time() )
    					);
                        
                comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'bwskal' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
                printf('</span></h4>');    
				?>	
                <?php if ( '0' == $comment->comment_approved ) : ?>
    				<p class="comment-awaiting-moderation"><?php _e( 'Komentar Anda sedang dalam proses persetujuan', 'bwskal' ); ?></p>
    			<?php endif; ?>
                
    			<section class="comment-content comment">
    				<p><?php comment_text(); ?></p>
    				<?php edit_comment_link( __( 'Edit', 'bwskal' ), '<p class="edit-link">', '</p>' ); ?>
    			</section><!-- .comment-content -->
            </div>
            <hr />
		</article><!-- #comment-## -->
            
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

/**
 * Get rid of website input field
*/

    function remove_comment_fields($fields) {
        unset($fields['url']);
        return $fields;
    }
    add_filter('comment_form_default_fields','remove_comment_fields');
    
/**
 * Self Pingback avoidance
*/
    function disable_self_ping( &$links ) {
        foreach ( $links as $l => $link )
            if ( 0 === strpos( $link, get_option( 'home' ) ) )
                unset($links[$l]);
    }
    add_action( 'pre_ping', 'disable_self_ping' );
    
/**
 * activate featured image
 */
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'berita-thumb', 240, 180, true ); // Hard Crop Mode
/**
 * Automatic featured image
 * A post cannot hold the same featured image from media except the image of bwskal2 whis is the default featured image for the post
 */
 function autoset_featured() 
    {
      global $post;
      $already_has_thumb = has_post_thumbnail($post->ID);
          if (!$already_has_thumb)  {
          $attached_image = get_children( "post_parent=$post->ID&post_type=attachment&post_mime_type=image&numberposts=1" );
                      if ($attached_image) {
                            foreach ($attached_image as $attachment_id => $attachment) {
                            set_post_thumbnail($post->ID, $attachment_id);
                            }
                       }
                      else 
                      {
                        set_post_thumbnail($post->ID, '94');
                      }
                    }
      }
    add_action('the_post', 'autoset_featured');
    add_action('save_post', 'autoset_featured');
    add_action('draft_to_publish', 'autoset_featured');
    add_action('new_to_publish', 'autoset_featured');
    add_action('pending_to_publish', 'autoset_featured');
    add_action('future_to_publish', 'autoset_featured');
    
    /**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function add_custom_taxonomies() {
  // Add new "Locations" taxonomy to Posts
  register_taxonomy('kategori', 'pekerjaan', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Kategori', 'taxonomy general name' ),
      'singular_name' => _x( 'Kategori', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Kategori Potensidan Prasarana' ),
      'all_items' => __( 'Semua Kategori' ),
      'parent_item' => __( 'Parent Kategori' ),
      'parent_item_colon' => __( 'Parent Kategori:' ),
      'edit_item' => __( 'Edit Kategori' ),
      'update_item' => __( 'Update Kategori' ),
      'add_new_item' => __( 'Tambah Kategori Baru' ),
      'new_item_name' => __( 'Nama Kategori Baru' ),
      'menu_name' => __( 'Kategori' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'kategori', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_custom_taxonomies', 0 );

/**
 * Add my own theme setting
 * 
 */

// Hook for adding admin menus
if ( is_admin() ){ // admin actions
  add_action( 'admin_menu', 'mt_add_pages' );
  add_action( 'admin_init', 'plugin_admin_init' );
} else {
  // non-admin enqueues, actions, and filters
}

// action function for above hook
function mt_add_pages() {
    // Add a new submenu under Settings:
    add_theme_page(__('Test Settings','menu-test'), __('Test Settings','menu-test'), 'manage_options', 'testsettings', 'mt_settings_page');
}

function plugin_admin_init() { // whitelist options
  register_setting( 'myoption-group', 'banner' );
  register_setting( 'myoption-group', 'some_other_option' );
  add_settings_section('plugin_main', 'Main Settings', 'plugin_section_text', 'plugin');
  add_settings_field('plugin_text_string', 'Plugin Text Input', 'plugin_setting_string', 'plugin', 'plugin_main');
}

// mt_settings_page() displays the page content for the Test settings submenu
function mt_settings_page() {
    if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
?>    
    <div class="wrap">
    <h2>Pengaturan Tema BWSKAL2</h2>
    
    <form method="post" action="options.php">
        <?php settings_fields( 'baw-settings-group' ); ?>
        <?php do_settings_sections( 'baw-settings-group' ); ?>
        <table class="form-table">
            <tr valign="top">
            <th scope="row">New Option Name</th>
            <td><input type="file" name="banner" value="<?php echo esc_attr( get_option('banner') ); ?>" /></td>
            </tr>
             
            <tr valign="top">
            <th scope="row">Some Other Option</th>
            <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
            </tr>
            
        </table>
        
        <?php submit_button(); ?>
    
    </form>
    </div>
<?php } ?>