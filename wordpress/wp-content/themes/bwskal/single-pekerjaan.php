<?php
    /*Template Name: detail_pekerjaan
    Theme Name : bwskal2*/
?>
<?php include("header.php") ?>
<!-- BEGIN CONTAINER -->   
<div class="page-container row-fluid">
    <!-- BEGIN PAGE -->
    <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12 page-berita">
                        <div class="container">
                            <div class="span9 left-content">
                                <div class="news-detail">
                                    <div class="row-fluid">
                                    <?php while ( have_posts() ) : the_post(); ?>
                                         <div class="span12 news-detail-content">
                                            <h1><?php the_title(); ?></h1>
            								<div class="row-fluid">
            									<div class="span6">
                                                    <?php the_tags('<ul class="unstyled inline blog-tags"><i class="icon-tags" style="color:#78cff8"></i><li>','','</li></ul>'); ?>
                                                </div>
                                                <div class="span6 blog-tag-data-inner">
                                                    <ul class="unstyled inline">
                                                        <li><i class="icon-calendar"></i> <?php the_date(); ?></li>
                       									<li><i class="icon-comments"></i> <a href="<?php comments_link(); ?>"><?php comments_number('0 comment','1 comment','% comments'); ?></a></li>
                                                    </ul>
            									</div>
                                            </div>
                                            <p><?php the_content(); ?></p>
                                            <a class="btn blue" href="http://www.pu.go.id/site/view/67">Lelang Online</a>
              							</div>
                                     </div>
                                     <?php endwhile; ?>
                                 </div>
                                 <?php comments_template('',TRUE); ?>
					           </div>
                                <div class="span3 right-content">
                                    <div class="berita-lain">
                                        <h2>Pekerjaan Lain</h2>
                                        <ul class="unstyled">
                                            <li>
                                                <a  href="http://localhost/bwskal2/wordpress/?p=282"><h5><strong>Pekerjaan 5</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.lit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                            <li>
                                                <a  href="http://localhost/bwskal2/wordpress/?p=283"><h5><strong>Pekerjaan 6</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                            <li>
                                                <a href="http://localhost/bwskal2/wordpress/?p=284"><h5><strong>Pekerjaan 7</strong></h5></a>
                                                <img src="<?php bloginfo('template_directory')?>/img/image2.jpg"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere erat a ante.
                                                Donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE --> 
</div>
    <!-- END CONTAINER -->
<?php include("footer.php") ?>