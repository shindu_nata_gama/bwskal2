# Translation of Development in Indonesian
# This file is distributed under the same license as the Development package.
msgid ""
msgstr ""
"PO-Revision-Date: 2014-04-15 22:39:50+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/0.1\n"
"Project-Id-Version: Development\n"

#: views/start.php:77
msgid "Activate Akismet"
msgstr "Aktifkan Akismet"

#: views/start.php:64 views/start.php:85
msgid "If you already know your API key."
msgstr "Jika Anda sudah mengetahui kunci API Anda."

#: views/start.php:78
msgid "Log in or create an account to get your API key."
msgstr "Log masuk atau buat sebuah akun untuk memperoleh kunci API Anda."

#: views/start.php:80
msgid "Get your API key"
msgstr "Ambil kunci API Anda."

#: views/start.php:22
msgid "Your subscription for %s is cancelled"
msgstr "Langganan Anda untuk %s dibatalkan"

#: views/start.php:28
msgid "Reactivate Akismet"
msgstr "Aktifkan Ulang Akismet"

#: views/config.php:136
msgid "Cancelled"
msgstr "Dibatalkan"

#: views/config.php:138
msgid "Suspended"
msgstr "Ditangguhkan"

#: views/config.php:140
msgid "Missing"
msgstr "Tak ditemukan"

#: views/config.php:142
msgid "No Subscription Found"
msgstr "Langganan Tak Ditemukan"

#: views/config.php:144
msgid "Active"
msgstr "Aktif"

#: views/notice.php:66
msgid "There is a problem with your key."
msgstr "Ada masalah dengan kunci Anda."

#: views/notice.php:72
msgid "Since 2012, Akismet began using subscriptions for all accounts (even free ones). It looks like a subscription has not been assigned to your account, and we’d appreciate it if you’d <a href=\"%s\" target=\"_blank\">sign into your account</a> and choose one. Please <a href=\"%s\" target=\"_blank\">contact our support team</a> with any questions."
msgstr "Sejak 2012, Akismet memberlakukan langganan untuk semua akun (termasuk yang gratis). Kelihatannya belum ada paket langganan terkait akun Anda, dan kami akan berterimakasih jika Anda <a href=\"%s\" target=\"_blank\">mendaftarkan akun</a> dan memilih satu paket. Silakan <a href=\"%s\" target=\"_blank\">hubungi tim dukungan kami</a> jika ada pertanyaan."

#: class.akismet-admin.php:194 views/config.php:84
msgid "Strictness"
msgstr "Ketegasan"

#: class.akismet-admin.php:194
msgid "Choose to either discard the worst spam automatically or to always put all spam in spam folder."
msgstr "Pilih untuk membuang spam terburuk secara otomatis atau untuk menyimpan spam di folder spam."

#: views/config.php:88
msgid "Silently discard the worst and most pervasive spam so I never see it."
msgstr "Buang spam terburuk dan terulet secara diam-diam agar saya tak pernah melihatnya."

#: views/config.php:89
msgid "Always put spam in the Spam folder for review."
msgstr "Selalu masukkan spam ke folder Spam untuk saya tinjau."

#: class.akismet-admin.php:193 views/config.php:75
msgid "Comments"
msgstr "Komentar"

#: views/notice.php:71
msgid "Your subscription is missing."
msgstr "Langganan Anda tidak ditemukan."

#: views/notice.php:89
msgid "You're using your Akismet key on more sites than your Pro subscription allows."
msgstr "Anda menggunakan kunci Akismet Anda pada lebih banyak situs daripada yang diizinkan oleh langganan Pro."

#: views/notice.php:90
msgid "If you would like to use Akismet on more than 10 sites, you will need to <a href=\"%s\" target=\"_blank\">upgrade to an Enterprise subscription</a>. If you have any questions, please <a href=\"%s\" target=\"_blank\">get in touch with our support team</a>"
msgstr "Jika Anda ingin menggunakan Akismet untuk lebih dari 10 situs, Anda perlu <a href=\"%s\" target=\"_blank\">meningkatkan ke paket Enterprisen</a>. Jika Anda memiliki pertanyaan, silakan <a href=\"%s\" target=\"_blank\">hubungi tim dukungan kami</a>"

#: views/notice.php:92
msgid "You're using Akismet on far too many sites for your Pro subscription."
msgstr "Anda menggunakan Akismet pada terlalu banyak situs untuk langganan Pro Anda."

#: views/notice.php:93
msgid "To continue your service, <a href=\"%s\" target=\"_blank\">upgrade to an Enterprise subscription</a>, which covers an unlimited number of sites. Please <a href=\"%s\" target=\"_blank\">contact our support team</a> with any questions."
msgstr "Untuk melanjutkan layanan Anda, <a href=\"%s\" target=\"_blank\">tingkatkan ke paket Enterprise</a>, yang melayani situs dalam jumlah tak terbatas. Silakan <a href=\"%s\" target=\"_blank\">hubungi tim dukungan kami</a> jika ada pertanyaan."

#: views/notice.php:19
msgid "Akismet has detected a problem."
msgstr "Akismet telah mendeteksi sebuah masalah."

#: views/notice.php:19
msgid "Some comments have not yet been checked for spam by Akismet. They have been temporarily held for moderation. Please check your <a href=\"%s\">Akismet configuration</a> and contact your web host if problems persist."
msgstr "Beberapa komentar belum diperiksa oleh Akismet. Mereka sementara ditahan untuk moderasi. Silahkan cek <a href=\"%s\">konfigurasi Akismet</a> dan hubungi web host Anda jika masalah berlanjut."

#: views/notice.php:21
msgid "Akismet %s requires WordPress 3.0 or higher."
msgstr "Akismet %s membutuhkan WordPress 3.0 atau lebih tinggi."

#: views/notice.php:24
msgid "Akismet Error Code: %s"
msgstr "Kode Galat Akismet: %s"

#: views/notice.php:29
msgid "For more information: %s"
msgstr "Informasi lebih lanjut: %s"

#: views/notice.php:36
msgid "Network functions are disabled."
msgstr "Fungsi jaringan dimatikan."

#: views/notice.php:37
msgid "Your web host or server administrator has disabled PHP&#8217;s <code>fsockopen</code> or <code>gethostbynamel</code> functions.  <strong>Akismet cannot work correctly until this is fixed.</strong>  Please contact your web host or firewall administrator and give them <a href=\"%s\" target=\"_blank\">this information about Akismet&#8217;s system requirements</a>."
msgstr "Administrator web host atau server Anda telah menonaktifkan fungsi PHP <code>fsockopen</code> atau <code>gethostbynamel</code>. <strong>Akismet tidak bisa bekerja dengan baik sebelum ini diatasi.</strong> Silakan hubungi administrator web host atau firewall Anda dan sampaikan <a href=\"%s\" target=\"_blank\">informasi tentang prasyarat sistem Akismet ini</a>."

#: views/notice.php:41
msgid "We can&#8217;t connect to your site."
msgstr "Kami tak bisa menyambungkan situs Anda."

#: views/notice.php:42
msgid "Your firewall may be blocking us. Please contact your host and refer to <a href=\"%s\" target=\"_blank\">our guide about firewalls</a>."
msgstr "Firewall Anda mungkin memblokir kami. Silakan kontak host Anda dan berikan rujukan ke <a href=\"%s\" target=\"_blank\">panduan firewall kami</a>."

#: views/notice.php:46
msgid "Please update your payment details."
msgstr "Silakan mutakhirkan rincian pembayaran Anda."

#: views/notice.php:47
msgid "We cannot process your transaction. Please contact your bank for assistance, and <a href=\"%s\" target=\"_blank\">update your payment details</a>."
msgstr "Kami tak bisa memproses transaksi Anda. Silakan hubungi bank Anda untuk asistensi, dan <a href=\"%s\" target=\"_blank\">mutakhirkan rincian pembayaran Anda</a>."

#: views/notice.php:51
msgid "Your subscription is cancelled."
msgstr "Langganan Anda dibatalkan."

#: views/notice.php:52
msgid "Please visit the <a href=\"%s\" target=\"_blank\">Akismet account page</a> to reactivate your subscription."
msgstr "Silakan kunjungi <a href=\"%s\" target=\"_blank\">laman akun Akismet</a> untuk mengaktifkan ulang langganan Anda."

#: views/notice.php:56
msgid "Your subscription is suspended."
msgstr "Langganan Anda ditangguhkan."

#: views/notice.php:57 views/notice.php:67
msgid "Please contact <a href=\"%s\" target=\"_blank\">Akismet support</a> for assistance."
msgstr "Silakan hubungi <a href=\"%s\" target=\"_blank\">dukungan Akismet</a> untuk asistensi."

#: views/notice.php:62
msgid "You can help us fight spam and upgrade your account by <a href=\"%s\" target=\"_blank\">contributing a token amount</a>."
msgstr "Anda bisa membantu kami memerangi spam dan meningkatkan akun Anda dengan cara <a href=\"%s\" target=\"_blank\">mengkontribusikan sejumlah token</a>."

#: views/notice.php:76
msgid "Your Akismet account has been successfully set up and activated. Happy blogging!"
msgstr "Akun Aksimet Anda telah sukses dikelola dan telah diaktifkan. Selamat nge-blog !"

#: views/notice.php:80
msgid "The key you entered is invalid. Please double-check it."
msgstr "Kunci yang Anda masukkan tidak sah. Coba periksa kembali."

#: views/notice.php:84
msgid "The key you entered could not be verified because a connection to akismet.com could not be established. Please check your server configuration."
msgstr "Kunci yang Anda masukkan tidak bisa diverifikasi karena koneksi ke akismet.com tidak dapat dibangun. Mohon periksa konfigurasi server Anda."

#: views/start.php:40 views/start.php:74
msgid "Akismet eliminates the comment and trackback spam you get on your site. To setup Akismet, select one of the options below."
msgstr "Akismet mengeliminir komentar dan trackback spam yang masuk ke situs Anda. Untuk mengatur Akismet, pilih salah satu opsi di bawah ini."

#: views/start.php:7 views/start.php:21 views/start.php:34 views/start.php:43
msgid "Connected via Jetpack"
msgstr "Tersambung via Jetpack"

#: views/start.php:50
msgid "Use this Akismet account"
msgstr "Pakai aku Akismet ini"

#: views/start.php:56
msgid "Create a new API key with a different email address"
msgstr "Buat API key baru dengan alamat email berbeda"

#: views/start.php:57
msgid "Use this option if you want to setup a new Akismet account."
msgstr "Pilih opsi ini untuk membuat akun Akismet baru."

#: views/start.php:59
msgid "Register a different email address"
msgstr "Daftarkan alamat email lain"

#: views/start.php:63 views/start.php:84
msgid "Manually enter an API key"
msgstr "Masukkan API key secara manual"

#: views/start.php:70 views/start.php:91
msgid "Use this key"
msgstr "Gunakan key ini"

#: views/start.php:4
msgid "Akismet eliminates the comment and trackback spam you get on your site. Register your email address below to get started."
msgstr "Akismet mengeliminir komentar dan trackback spam yang masuk ke situs Anda. Daftarkan alamat email di bawah ini untuk memulainya."

#: views/start.php:14
msgid "Register Akismet"
msgstr "Daftarkan Akismet"

#: views/start.php:18 views/start.php:32
msgid "Akismet eliminates the comment and trackback spam you get on your site."
msgstr "Akismet mengeliminir komentar dan trackback spam yang masuk ke situs Anda."

#: views/start.php:35
msgid "Your subscription for %s is suspended"
msgstr "Langganan Anda untuk %s ditangguhkan"

#: views/start.php:36
msgid "No worries! Get in touch and we&#8217;ll help sort this out."
msgstr "Tak perlu khawatir! Hubungi kami untuk menyelesaikan masalah ini."

#: views/start.php:37
msgid "Contact Akismet support"
msgstr "Hubungi dukungan Akismet"

#: views/config.php:87 views/strict.php:2 views/strict.php:3
msgid "Akismet anti-spam strictness"
msgstr "Ketegasan anti-spam Akismet"

#: views/strict.php:4
msgid "Strict: silently discard the worst and most pervasive spam."
msgstr "Tegas: membuang spam terburuk dan terulet secara diam-diam."

#: views/strict.php:5
msgid "Safe: always put spam in the Spam folder for review."
msgstr "Aman: selalu memasukkan spam ke folder Spam untuk peninjauan."

#: class.akismet-admin.php:51
msgid "Comment History"
msgstr "Sejarah Komentar"

#: class.akismet-admin.php:74 class.akismet-admin.php:76
#: class.akismet-admin.php:850 views/config.php:3
msgid "Akismet"
msgstr "Akismet"

#: class.akismet-admin.php:102
msgid "Remove this URL"
msgstr "Hapus URL ini"

#: class.akismet-admin.php:103
msgid "Removing..."
msgstr "Menghapus..."

#: class.akismet-admin.php:104
msgid "URL removed"
msgstr "URL dihapus"

#: class.akismet-admin.php:105
msgid "(undo)"
msgstr "(undo)"

#: class.akismet-admin.php:106
msgid "Re-adding..."
msgstr "Menambahkan kembali..."

#: class.akismet-admin.php:127 class.akismet-admin.php:165
#: class.akismet-admin.php:178
msgid "Overview"
msgstr "Ikhtisar"

#: class.akismet-admin.php:129 class.akismet-admin.php:140
#: class.akismet-admin.php:151
msgid "Akismet Setup"
msgstr "Pengaturan Akismet"

#: class.akismet-admin.php:130 class.akismet-admin.php:168
#: class.akismet-admin.php:181
msgid "Akismet filters out your comment and trackback spam for you, so you can focus on more important things."
msgstr "Akismet menyaring komentar dan trackback spam untuk Anda, sehingga Anda bisa fokus untuk hal-hal yang lebih penting."

#: class.akismet-admin.php:131
msgid "On this page, you are able to setup the Akismet plugin."
msgstr "Di laman ini, Anda bisa mengatur plugin Akismet."

#: class.akismet-admin.php:138
msgid "New to Akismet"
msgstr "Baru kenal Akismet"

#: class.akismet-admin.php:141
msgid "You need to enter an API key to activate the Akismet service on your site."
msgstr "Anda butuh kunci API untuk mengaktifkan layanan Akismet pada situs Anda."

#: class.akismet-admin.php:142
msgid "Signup for an account on %s to get an API Key."
msgstr "Daftarkan sebuah akun pada %s untuk memperoleh kunci API."

#: class.akismet-admin.php:149
msgid "Enter an API Key"
msgstr "Masukkan Kunci API"

#: class.akismet-admin.php:152
msgid "If you already have an API key"
msgstr "Jika Anda sudah punya kunci API"

#: class.akismet-admin.php:154
msgid "Copy and paste the API key into the text field."
msgstr "Salin dan sematkan kunci API tersebut ke dalam bidang teks."

#: class.akismet-admin.php:155
msgid "Click the Use this Key button."
msgstr "Klik tombol Gunakan Kunci Ini."

#: class.akismet-admin.php:167 views/stats.php:2
msgid "Akismet Stats"
msgstr "Statistik Akismet"

#: class.akismet-admin.php:169
msgid "On this page, you are able to view stats on spam filtered on your site."
msgstr "Di laman ini, Anda bisa melihat statistik spam yang tersaring pada situs Anda."

#: class.akismet-admin.php:180 class.akismet-admin.php:191
#: class.akismet-admin.php:203
msgid "Akismet Configuration"
msgstr "Konfigurasi Akismet"

#: class.akismet-admin.php:182
msgid "On this page, you are able to enter/remove an API key, view account information and view spam stats."
msgstr "Di laman ini, Anda bisa memasukkan/menghapus kunci API, melihat informasi akun dan statistik spam."

#: class.akismet-admin.php:67 class.akismet-admin.php:189
#: class.akismet-admin.php:542 views/config.php:60 views/stats.php:2
msgid "Settings"
msgstr "Pengaturan"

#: class.akismet-admin.php:192 views/config.php:67
msgid "API Key"
msgstr "Kunci API"

#: class.akismet-admin.php:192
msgid "Enter/remove an API key."
msgstr "Masukkan/hapus kunci API."

#: views/config.php:79
msgid "Show the number of approved comments beside each comment author"
msgstr "Tampilkan jumlah komentar yang disetujui di samping masing-masing komentator."

#: class.akismet-admin.php:193
msgid "Show the number of approved comments beside each comment author in the comments list page."
msgstr "Tampilkan jumlah komentar yang disetujui di samping masing-masing komentator pada laman daftar komentar."

#: class.akismet-admin.php:201 views/config.php:119
msgid "Account"
msgstr "Akun"

#: class.akismet-admin.php:204 views/config.php:124
msgid "Subscription Type"
msgstr "Tipe Langganan"

#: class.akismet-admin.php:204
msgid "The Akismet subscription plan"
msgstr "Paket langganan Akismet."

#: class.akismet-admin.php:205 views/config.php:131
msgid "Status"
msgstr "Status"

#: class.akismet-admin.php:205
msgid "The subscription status - active, cancelled or suspended"
msgstr "Status langganan - aktif, dibatalkan, atau ditangguhkan"

#: class.akismet-admin.php:213
msgid "For more information:"
msgstr "Untuk informasi lebih lanjut:"

#: class.akismet-admin.php:214
msgid "Akismet FAQ"
msgstr "Akismet FAQ"

#: class.akismet-admin.php:215
msgid "Akismet Support"
msgstr "Dukungan Akismet"

#: class.akismet-admin.php:221
msgid "Cheatin&#8217; uh?"
msgstr "Mau curang ya?"

#: class.akismet-admin.php:280
msgctxt "comments"
msgid "Spam"
msgstr "Spam"

#: class.akismet-admin.php:282
msgid "<a href=\"%1$s\">Akismet</a> has protected your site from <a href=\"%2$s\">%3$s spam comment</a>."
msgid_plural "<a href=\"%1$s\">Akismet</a> has protected your site from <a href=\"%2$s\">%3$s spam comments</a>."
msgstr[0] "<a href=\"%1$s\">Akismet</a> telah melindungi situs Anda dari <a href=\"%2$s\">%3$s komentar spam</a>."
msgstr[1] "<a href=\"%1$s\">Akismet</a> telah melindungi situs Anda dari <a href=\"%2$s\">%3$s komentar spam</a>."

#: class.akismet-admin.php:301
msgid "<a href=\"%1$s\">Akismet</a> has protected your site from %2$s spam comment already. "
msgid_plural "<a href=\"%1$s\">Akismet</a> has protected your site from %2$s spam comments already. "
msgstr[0] "<a href=\"%1$s\">Akismet</a> telah berhasil melindungi situs anda dari %2$s komentar spam."
msgstr[1] "<a href=\"%1$s\">Akismet</a> telah berhasil melindungi situs anda dari %2$s komentar spam."

#: class.akismet-admin.php:307
msgid "<a href=\"%s\">Akismet</a> blocks spam from getting to your blog. "
msgstr "<a href=\"%s\">Akismet</a> memblokir spam yang akan masuk ke blog Anda. "

#: class.akismet-admin.php:312
msgid "There&#8217;s <a href=\"%2$s\">%1$s comment</a> in your spam queue right now."
msgid_plural "There are <a href=\"%2$s\">%1$s comments</a> in your spam queue right now."
msgstr[0] "Ada <a href=\"%2$s\">%1$s komentar</a> dalam antrean spam Anda saat ini."
msgstr[1] "Ada <a href=\"%2$s\">%1$s komentar</a> dalam antrean spam Anda saat ini."

#: class.akismet-admin.php:318
msgid "There&#8217;s nothing in your <a href='%s'>spam queue</a> at the moment."
msgstr "Saat ini, <a href='%s'>antrean spam</a> Anda kosong."

#: class.akismet-admin.php:334
msgid "Check for Spam"
msgstr "Periksa akan Spam"

#: class.akismet-admin.php:378
msgid "%1$s changed the comment status to %2$s"
msgstr "%1$s mengubah status komentar menjadi %2$s"

#: class.akismet-admin.php:419
msgid "Akismet re-checked and caught this comment as spam"
msgstr "Akismet memeriksa kembali dan menangkap komentar ini sebagai spam"

#: class.akismet-admin.php:425
msgid "Akismet re-checked and cleared this comment"
msgstr "Akismet memeriksa kembali dan membersihkan komentar ini"

#: class.akismet-admin.php:429
msgid "Akismet was unable to re-check this comment (response: %s)"
msgstr "Akismet tidak dapat memeriksa kembali komentar ini (respon:%s)"

#: class.akismet-admin.php:483
msgid "Awaiting spam check"
msgstr "Menunggu pemeriksaan spam"

#: class.akismet-admin.php:487
msgid "Flagged as spam by Akismet"
msgstr "Ditandai sebagai spam oleh Akismet"

#: class.akismet-admin.php:489
msgid "Cleared by Akismet"
msgstr "Dibersihkan oleh Akismet"

#: class.akismet-admin.php:493
msgid "Flagged as spam by %s"
msgstr "Ditandai sebagai spam oleh %s"

#: class.akismet-admin.php:495
msgid "Un-spammed by %s"
msgstr "Pembatalan spam oleh %s"

#: class.akismet-admin.php:507 class.akismet-admin.php:515
msgid "View comment history"
msgstr "Lihat sejarah komentar"

#: class.akismet-admin.php:507
msgid "History"
msgstr "Sejarah"

#: class.akismet-admin.php:520
msgid "%s approved"
msgid_plural "%s approved"
msgstr[0] "%s disetujui"
msgstr[1] "%s disetujui"

#: class.akismet-admin.php:533
msgid "%s ago"
msgstr "%s lalu"

#: class.akismet-admin.php:602
msgid "%s reported this comment as spam"
msgstr "%s melaporkan komentar ini sebagai spam"

#: class.akismet-admin.php:648
msgid "%s reported this comment as not spam"
msgstr "%s melaporkan komentar ini bukan spam"

#: class.akismet-admin.php:880
msgid "Cleaning up spam takes time."
msgstr "Membersihkan spam menghabiskan waktu."

#: class.akismet-admin.php:883
msgid "Since you joined us, Akismet has saved you %s days!"
msgstr "Sejak bergabung dengan kami, Akismet telah menghemat %s hari Anda!"

#: class.akismet-admin.php:885
msgid "Since you joined us, Akismet has saved you %d hours!"
msgstr "Sejak bergabung dengan kami, Akismet telah menghemat %d jam Anda!"

#: class.akismet-admin.php:887
msgid "Since you joined us, Akismet has saved you %d minutes!"
msgstr "Sejak bergabung dengan kami, Akismet telah menghemat %d menit Anda!"

#: class.akismet-widget.php:12
msgid "Akismet Widget"
msgstr "Widget Akismet"

#: class.akismet-widget.php:13
msgid "Display the number of spam comments Akismet has caught"
msgstr "Tampilkan jumlah komentar spam yang ditangkap Akismet"

#: class.akismet-widget.php:69
msgid "Spam Blocked"
msgstr "Spam Diblokir"

#: class.akismet-widget.php:74
msgid "Title:"
msgstr "Judul:"

#: class.akismet-widget.php:98
msgid "<strong class=\"count\">%1$s spam</strong> blocked by <strong>Akismet</strong>"
msgid_plural "<strong class=\"count\">%1$s spam</strong> blocked by <strong>Akismet</strong>"
msgstr[0] "<strong class=\"count\">%1$s spam</strong> diblok oleh <strong>Akismet</strong>"
msgstr[1] "<strong class=\"count\">%1$s spam</strong> diblok oleh <strong>Akismet</strong>"

#: class.akismet.php:220
msgid "Akismet caught this comment as spam"
msgstr "Akismet menangkap komentar ini sebagai spam"

#: class.akismet.php:222 class.akismet.php:231
msgid "Comment status was changed to %s"
msgstr "Status komentar telah diganti menjadi %s"

#: class.akismet.php:226
msgid "Akismet cleared this comment"
msgstr "Akismet membersihkan komentar ini"

#: class.akismet.php:229
msgid "Comment was caught by wp_blacklist_check"
msgstr "Komentar tertangkap wp_blacklist_check"

#: class.akismet.php:236
msgid "Akismet was unable to check this comment (response: %s), will automatically retry again later."
msgstr "Akismet tidak dapat mengecek komentar ini (respon: %s), akan dicoba kembali secara otomatis kemudian."

#: class.akismet.php:403
msgid "Akismet caught this comment as spam during an automatic retry."
msgstr "Akismet menangkap komentar ini sebagai spam pada saat percobaan kembali secara otomatis."

#: class.akismet.php:405
msgid "Akismet cleared this comment during an automatic retry."
msgstr "Akismet membersihkan komentar ini pada saat percobaan kembali secara otomatis."

#: class.akismet.php:746
msgid "Akismet %s requires WordPress %s or higher."
msgstr "Akismet %s membutuhkan WordPress %s atau selebihnya."

#: class.akismet.php:746 views/notice.php:21
msgid "Please <a href=\"%1$s\">upgrade WordPress</a> to a current version, or <a href=\"%2$s\">downgrade to version 2.4 of the Akismet plugin</a>."
msgstr "Silakan <a href=\"%1$s\">mutakhirkan WordPress</a> ke versi terbaru, atau <a href=\"%2$s\">turunkan plugin Akismet ke versi 2.4</a>."

#: views/config.php:12
msgid "Summaries"
msgstr "Ringkasan"

#: views/config.php:18
msgid "Past six months"
msgstr "Enam bulan belakangan"

#: views/config.php:20 views/config.php:25
msgid "Spam blocked"
msgstr "Spam diblokir"

#: views/config.php:23
msgid "All time"
msgstr "Sepanjang waktu"

#: views/config.php:28
msgid "Accuracy"
msgstr "Akurasi"

#: views/config.php:32
msgid "%s missed spam, %s false positive"
msgid_plural "%s missed spam, %s false positives"
msgstr[0] "%s spam lolos, %s komentar palsu"
msgstr[1] "%s spam lolos, %s komentar palsu"

#: views/config.php:79
msgid "Show approved comments"
msgstr "Perlihatkan komentar yang telah disetujui"

#: views/config.php:91
msgid "Note:"
msgstr "Catatan:"

#: views/config.php:91
msgid "Spam in the <a href=\"%s\">spam folder</a> older than 15 days is deleted automatically."
msgstr "Spam dalam <a href=\"%s\">folder Spam</a> yang berusia lebih dari 15 hari akan dihapus secara otomatis."

#: views/config.php:100
msgid "Disconnect this account"
msgstr "Putuskan sambungan akun ini"

#: views/config.php:106
msgid "Save Changes"
msgstr "Simpan Perubahan"

#: views/config.php:150
msgid "Next Billing Date"
msgstr "Tanggal Tagihan Berikutnya"

#: views/config.php:162
msgid "Upgrade"
msgstr "Tingkatkan"

#: views/config.php:162
msgid "Change"
msgstr "Ubah"

#: views/notice.php:11
msgid "Activate your Akismet account"
msgstr "Aktifkan akun Askimet Anda"

#: views/notice.php:14
msgid "<strong>Almost done</strong> - activate your account and say goodbye to comment spam"
msgstr "<strong>Hampir selesai</strong> - Aktifkan akun Anda dan katakan selamat tinggal kepada komentar spam"