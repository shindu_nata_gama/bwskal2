<?php
/** 
 * Konfigurasi dasar WordPress.
 *
 * Berkas ini berisi konfigurasi-konfigurasi berikut: Pengaturan MySQL, Awalan Tabel,
 * Kunci Rahasia, Bahasa WordPress, dan ABSPATH. Anda dapat menemukan informasi lebih
 * lanjut dengan mengunjungi Halaman Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Menyunting wp-config.php}. Anda dapat memperoleh pengaturan MySQL dari web host Anda.
 *
 * Berkas ini digunakan oleh skrip penciptaan wp-config.php selama proses instalasi.
 * Anda tidak perlu menggunakan situs web, Anda dapat langsung menyalin berkas ini ke
 * "wp-config.php" dan mengisi nilai-nilainya.
 *
 * @package WordPress
 */

// ** Pengaturan MySQL - Anda dapat memperoleh informasi ini dari web host Anda ** //
/** Nama basis data untuk WordPress */
define('DB_NAME', 'bwskal2');

/** Nama pengguna basis data MySQL */
define('DB_USER', 'root');

/** Kata sandi basis data MySQL */
define('DB_PASSWORD', '');

/** Nama host MySQL */
define('DB_HOST', 'localhost');

/** Set Karakter Basis Data yang digunakan untuk menciptakan tabel basis data. */
define('DB_CHARSET', 'utf8');

/** Jenis Collate Basis Data. Jangan ubah ini jika ragu. */
define('DB_COLLATE', '');

/**#@+
 * Kunci Otentifikasi Unik dan Garam.
 * 
 * Ubah baris berikut menjadi frase unik!
 * Anda dapat menciptakan frase-frase ini menggunakan {@link https://api.wordpress.org/secret-key/1.1/salt/ Layanan kunci-rahasia WordPress.org}
 * Anda dapat mengubah baris-baris berikut kapanpun untuk mencabut validasi seluruh cookies. Hal ini akan memaksa seluruh pengguna untuk masuk log ulang.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZMJ=k1G9+B5cF@Gc}Htko)|uZ}&d3}0qTb;8gyLz<Ba/BjJ37Fy*jm[3qR()z4-D');
define('SECURE_AUTH_KEY',  '.=bGP*jM*w>I[o/h{gnQ0O!~+B1h)QONQU1yD=D%.qEA&+I$HGn6RJ?%I6PR&D&q');
define('LOGGED_IN_KEY',    'ji4k=.<ZOx03@$rnvXZzuDzuEgaeq%XH<vSaws@haX:U;MoyHj8&GolI`c82v%^Q');
define('NONCE_KEY',        'm2d*]{<.qQWSv8;NadtD hkdU1QPN%d)X7EHUz:-|IT&>N8IU/Oobc8MJ->(Lw:{');
define('AUTH_SALT',        '*;KnAe3;O>uAt5YPPw.XbZNU[;p{9Ih3S`L3JGzqJXGtB2Q0vk|IYp)Ne0[~iUA*');
define('SECURE_AUTH_SALT', '%dmRuP^-KCw!ENp<6_niN.kE^K8RD,08BQnp4)A8S#>~o;Ojbt>#PE(G#<]ly;~]');
define('LOGGED_IN_SALT',   'ixk[C+ESY)W8q0j_TfG:.}>tuZbP4LEepA?_QBQR.!WA#{BKBD|v,4JS5^<*1#a=');
define('NONCE_SALT',       '{D6~LNr>*5`Ty6q-:@a+t#(h[Cc<|eV4.;z>w=s*NYq-q9p:?EiJ7nlwg!hIT5qB');

/**#@-*/

/**
 * Awalan Tabel Basis Data WordPress.
 * 
 * Anda dapat memiliki beberapa instalasi di dalam satu basis data jika Anda memberikan awalan unik 
 * kepada masing-masing tabel. Harap hanya masukkan angka, huruf, dan garis bawah!
 */
$table_prefix  = 'wp_';

/**
 * Bahasa Lokal WordPress. Bila nilainya kosong secara standar Bahasa Inggris akan digunakan.
 *
 * Ubah ini untuk melokalkan WordPress. Berkas MO yang bersangkutan harus 
 * diinstal di wp-content/languages. Sebagai contoh, instal
 * id_ID.mo ke wp-content/languages dan tentukan WPLANG ke "id_ID" untuk mengaktifkan
 * dukungan Bahasa Indonesia. Dalam paket distribusi ini berkas id_ID.mo sudah terinstal.
 */
define ('WPLANG', 'id_ID');

/**
 * Untuk pengembang: Moda pengawakutuan WordPress.
 *
 * Ubah ini menjadi "true" untuk mengaktifkan tampilan peringatan selama pengembangan.
 * Sangat disarankan agar pengembang plugin dan tema menggunakan WP_DEBUG
 * di lingkungan pengembangan mereka.
define('WP_DEBUG', false);

/* Cukup, berhenti menyunting! Selamat ngeblog. */

/** Lokasi absolut direktori WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Menentukan variabel-variabel WordPress berkas-berkas yang disertakan. */
require_once(ABSPATH . 'wp-settings.php');
